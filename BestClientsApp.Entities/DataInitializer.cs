﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestClientsApp.Entities
{
    public class DataInitializer
    {
        public static List<Client> Clients { get; set; }

        static DataInitializer()
        {
            Clients = new List<Client>();
            List<ClientTask> tasks1 = new List<ClientTask>
            {
                new ClientTask(){ Name = "Visit1", ClientAdress = "6Th ave", Description = "The best visit of my life",StartDate = DateTime.Now,EndTime = DateTime.Now.AddDays(1) },
                new ClientTask(){ Name = "Visit2", ClientAdress = "5Th ave", Description = "Descr",StartDate = DateTime.Now,EndTime = DateTime.Now.AddDays(1) },
                new ClientTask(){ Name = "Visit3", ClientAdress = "9Th ave", Description = "",StartDate = DateTime.Now,EndTime = DateTime.Now.AddDays(1) },
                new ClientTask(){ Name = "Test1", ClientAdress = "9Th ave", Description = "",StartDate = DateTime.Now,EndTime = DateTime.Now.AddDays(1) },
                new ClientTask(){ Name = "TestName", ClientAdress = "9Th ave New York", Description = "",StartDate = DateTime.Now,EndTime = DateTime.Now.AddDays(1) },
                new ClientTask(){ Name = "Test2", ClientAdress = "Broklin", Description = "",StartDate = DateTime.Now,EndTime = DateTime.Now.AddDays(1) },
                new ClientTask(){ Name = "Visit", ClientAdress = "Independent sq", Description = "",StartDate = DateTime.Now,EndTime = DateTime.Now.AddDays(1) },
            };
            List<ClientTask> tasks2 = new List<ClientTask>
            {
                new ClientTask(){ Name = "Test Visit", ClientAdress = "home", Description = "It's a test description ",StartDate = DateTime.Now,EndTime = DateTime.Now.AddDays(1) },
                new ClientTask(){ Name = "Ann's Visit", ClientAdress = "Holywood", Description = "City stars",StartDate = DateTime.Now,EndTime = DateTime.Now.AddDays(1) },
                new ClientTask(){ Name = "It's a visit", ClientAdress = "Palm beach", Description = "",StartDate = DateTime.Now,EndTime = DateTime.Now.AddDays(1) },
               };

            var client1 = new Client() { FirstName = "Tomas", LastName = "Anderson", Adress = "NY", Number = 444777, Visits = tasks1 };
            var client2 = new Client() { FirstName = "Ann", LastName = "Smith", Adress = "California", Number = 123123, Visits = tasks2 };

            Clients.Add(client1);
            Clients.Add(client2);

        }
    }
}
