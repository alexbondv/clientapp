﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestClientsApp.Entities
{
    public class Client: BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Adress { get; set; }
        public int Number { get; set; }
        public List<ClientTask> Visits { get; set; }

        public Client()
        {
            Visits = new List<ClientTask>();
        }
    }
}
