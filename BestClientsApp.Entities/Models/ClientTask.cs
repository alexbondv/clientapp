﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestClientsApp.Entities
{
    public class ClientTask: BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string ClientAdress { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndTime { get; set; }
    }
}
