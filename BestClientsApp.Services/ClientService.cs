﻿using BestClientsApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestClientsApp.Services
{
    public class ClientService
    {
        private readonly DataMapper _mapper;
        private readonly CsvParser _csvParser;
        public ClientService()
        {
            _mapper = new DataMapper();
            _csvParser = new CsvParser();
            _csvParser.FormatToCsv();
        }

        public List<IndexClientViewModel> Get()
        {
            _csvParser.ReadFromCsv();
            var clients = _mapper.MapClientToIndexView(DataInitializer.Clients);
            return clients;
        }

        public List<GetClientTasksViewModel> GetTaskByClientId(string clientId)
        {
            var clientTasks = DataInitializer.Clients.FirstOrDefault(client => client.Id == Guid.Parse(clientId)).Visits;
            var view = _mapper.MapTasksToIndexView(clientTasks);
            return view;
        }

        public List<GetClientTasksViewModel> DeleteTaskById(string clientId, string taskId)
        {
            var currentClient = DataInitializer.Clients.FirstOrDefault(client => client.Id == Guid.Parse(clientId));
            currentClient.Visits.RemoveAll(e => e.Id == Guid.Parse(taskId));
          
            var view = _mapper.MapTasksToIndexView(currentClient.Visits);
            return view;

        }

    }
}
