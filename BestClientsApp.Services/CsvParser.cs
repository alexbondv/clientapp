﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using BestClientsApp.Entities;

namespace BestClientsApp.Services
{
    public class CsvParser
    {
        private string _filePath { get; set; }
        public CsvParser()
        {
            _filePath = $"{Directory.GetCurrentDirectory()}\\ClientNumbers.csv";
           
        }

        public void FormatToCsv()//Just for demo
        {
            StringBuilder csvContent = new StringBuilder();
            csvContent.AppendLine("Id,Client,Number");

            foreach (var client in DataInitializer.Clients)
            {
                csvContent.AppendLine($"{client.Id.ToString()},{client.FirstName.ToString()}, {client.Number.ToString()}");
            }
            File.AppendAllText(_filePath, csvContent.ToString());
        }

        public void ReadFromCsv()//Just for demo
        {
            try
            {
                var res = File.ReadAllText(_filePath);
                var values = res.Split(',');
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
