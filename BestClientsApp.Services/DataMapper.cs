﻿using BestClientsApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestClientsApp.Services
{
    public class DataMapper
    {
        public List<IndexClientViewModel> MapClientToIndexView(List<Client> clients)
        {
            var view = clients.Select(client => new IndexClientViewModel
            {
                Adress = client.Adress,
                FirstName = client.FirstName,
                ClientId = client.Id.ToString(),
                LastName = client.LastName,
                Number = client.Number
            }).ToList();
            return view;
        }

        public List<GetClientTasksViewModel> MapTasksToIndexView(List<ClientTask> tasks)
        {
            var view = tasks.Select(task => new GetClientTasksViewModel
            {
                TaskId = task.Id.ToString(),
                Name = task.Name,
                ClientAdress = task.ClientAdress,
                Description = task.Description,
                EndDate = task.EndTime,
                StartDate = task.StartDate
            }).ToList();
            return view;
        }
    }
}
