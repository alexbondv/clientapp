﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestClientsApp.Services
{
    public class GetClientTasksViewModel
    {
        public string TaskId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ClientAdress { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
