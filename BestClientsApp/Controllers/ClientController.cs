﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using BestClientsApp.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BestClientsApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private readonly ClientService _clientService;
        public ClientController()
        {
            _clientService = new ClientService();
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var view = _clientService.Get();
                return Ok(view);
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = "Something went wrong" });
                
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetTaskByClientId(string clientId)
        {
            try
            {
                var view = _clientService.GetTaskByClientId(clientId);
                return Ok(view);
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = "Something went wrong" });

            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(string clientId, string taskId)
        {
            try
            {
                var view = _clientService.DeleteTaskById(clientId,taskId);
                return Ok(view);              
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = "Something went wrong" });

            }
        }

    }
}